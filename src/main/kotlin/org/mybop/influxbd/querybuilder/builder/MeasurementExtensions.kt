package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name

fun measurement(name: String) = Measurement(Name(name))

fun measurement(retentionPolicy: String?, name: String) = Measurement(Name(name), retentionPolicy?.let { Name(it) })

fun measurement(database: String?, retentionPolicy: String?, name: String) = Measurement(Name(name), retentionPolicy?.let { Name(it) }, database?.let { Name(it) })

fun measurementDefaultRetention(database: String?, name: String) = Measurement(Name(name), null, database?.let { Name(it) })
