package org.mybop.influxbd.querybuilder.builder

fun select() = SelectBuilder()

fun select(init: SelectBuilder.() -> Unit = {}) =
        SelectBuilder().apply(init).build()
