package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.group.TimeGroup
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration
import org.mybop.influxbd.querybuilder.domain.time.instant.Now

fun time() = TimeBuilder()

fun time(duration: Duration) = TimeGroup(duration)

fun time(duration: String) = time(StringDuration(duration))

fun now() = Now
