package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.statement.CreateRetentionPolicy
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class CreateRetentionPolicyBuilder {

    private var name: String? = null

    private var database: String? = null

    private var duration: Duration? = null

    private var replication: Int? = null

    private var shardDuration: Duration? = null

    private var default: Boolean = false;

    infix fun named(name: String): CreateRetentionPolicyBuilder {
        this.name = name;
        return this
    }

    infix fun on(database: String): CreateRetentionPolicyBuilder {
        this.database = database
        return this
    }

    infix fun duration(duration: Duration): CreateRetentionPolicyBuilder {
        this.duration = duration
        return this
    }

    infix fun duration(duration: String) = duration(StringDuration(duration))

    infix fun replication(replication: Int): CreateRetentionPolicyBuilder {
        this.replication = replication
        return this
    }

    infix fun shardDuration(shardDuration: Duration): CreateRetentionPolicyBuilder {
        this.shardDuration = shardDuration
        return this
    }

    infix fun shardDuration(duration: String) = shardDuration(StringDuration(duration))

    fun default(default: Boolean = true): CreateRetentionPolicyBuilder {
        this.default = default
        return this
    }

    fun build() = CreateRetentionPolicy(
            policyName = Name(name!!),
            on = On(Name(database!!)),
            duration = duration!!,
            replication = replication!!,
            shardDuration = shardDuration,
            default = default
    )

    fun buildString() = build().toString()
}
