package org.mybop.influxbd.querybuilder.domain

import org.mybop.influxbd.querybuilder.domain.condition.TagCondition
import org.mybop.influxbd.querybuilder.domain.condition.TagOperator
import org.mybop.influxbd.querybuilder.domain.group.Dimension
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class Tag(name: String) : Key(name), Dimension {

    private fun condition(operator: TagOperator, value: StringValue) = TagCondition(tagKey = this, operator = operator, value = value)

    infix fun isEqualTo(other: StringValue) = condition(TagOperator.EQUAL_TO, other)

    infix fun isEqualTo(other: String) = isEqualTo(StringValue(other))

    infix fun isEqualTo(other: Boolean) = isEqualTo(StringValue(other.toString()))

    infix fun isEqualTo(other: Int) = isEqualTo(StringValue(other.toString()))

    infix fun isEqualTo(other: Long) = isEqualTo(StringValue(other.toString()))

    infix fun isEqualTo(other: Float) = isEqualTo(StringValue(other.toString()))

    infix fun isEqualTo(other: Double) = isEqualTo(StringValue(other.toString()))

    infix fun isEqualTo(other: Enum<*>) = isEqualTo(StringValue(other.name))

    infix fun isNotEqualTo(other: StringValue) = condition(TagOperator.NOT_EQUAL_TO, other)

    infix fun isNotEqualTo(other: String) = isNotEqualTo(StringValue(other))

    infix fun isNotEqualTo(other: Boolean) = isNotEqualTo(StringValue(other.toString()))

    infix fun isNotEqualTo(other: Int) = isNotEqualTo(StringValue(other.toString()))

    infix fun isNotEqualTo(other: Long) = isNotEqualTo(StringValue(other.toString()))

    infix fun isNotEqualTo(other: Float) = isNotEqualTo(StringValue(other.toString()))

    infix fun isNotEqualTo(other: Double) = isNotEqualTo(StringValue(other.toString()))

    infix fun isNotEqualTo(other: Enum<*>) = isNotEqualTo(StringValue(other.name))
}
