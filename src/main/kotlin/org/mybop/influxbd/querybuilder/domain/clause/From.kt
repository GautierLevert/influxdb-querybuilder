package org.mybop.influxbd.querybuilder.domain.clause

interface From {
    override fun toString(): String
}
