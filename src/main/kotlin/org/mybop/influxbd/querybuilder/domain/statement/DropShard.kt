package org.mybop.influxbd.querybuilder.domain.statement

class DropShard(
        val shardId: Int
) {
    override fun toString(): String {
        return "DROP SHARD $shardId"
    }
}
