package org.mybop.influxbd.querybuilder.domain.time.duration

import org.mybop.influxbd.querybuilder.domain.time.TimeArithmeticOperator

class CalculatedDuration(
        private val left: Duration,
        private val timeOperator: TimeArithmeticOperator,
        private val right: Duration
) : Duration {
    override fun toString() = "$left $timeOperator $right"
}
