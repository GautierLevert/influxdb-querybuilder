package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name

class ShowGrants(
        val user: Name
) {

    override fun toString(): String {
        return "SHOW GRANTS FOR $user"
    }
}
