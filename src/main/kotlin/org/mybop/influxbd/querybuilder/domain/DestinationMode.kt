package org.mybop.influxbd.querybuilder.domain

enum class DestinationMode {
    ANY,
    ALL;
}
