package org.mybop.influxbd.querybuilder.domain.selection

interface Selection {

    override fun toString(): String
}
