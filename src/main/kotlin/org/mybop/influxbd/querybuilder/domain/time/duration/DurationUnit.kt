package org.mybop.influxbd.querybuilder.domain.time.duration

enum class DurationUnit(val representation: String) {
    /**
     * 1 billionth of a second
     */
    NANOSECOND("ns"),
    /**
     * 1 millionth of a second)
     */
    MICROSECOND("us"),

    /**
     * 1 thousandth of a second
     */
    MILLISECOND("ms"),

    SECOND("s"),

    MINUTE("m"),

    HOUR("d"),

    DAY("d"),

    WEEK("w");
}
