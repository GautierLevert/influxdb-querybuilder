package org.mybop.influxbd.querybuilder.domain.statement

class ShowDatabases {

    override fun toString() = "SHOW DATABASES"
}
