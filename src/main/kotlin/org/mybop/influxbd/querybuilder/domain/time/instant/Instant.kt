package org.mybop.influxbd.querybuilder.domain.time.instant

import org.mybop.influxbd.querybuilder.domain.time.TimeArithmeticOperator
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

interface Instant {

    operator fun plus(duration: Duration) = CalculatedInstant(this, TimeArithmeticOperator.ADDITION, duration)

    operator fun plus(duration: String) = plus(StringDuration(duration))

    operator fun minus(duration: Duration) = CalculatedInstant(this, TimeArithmeticOperator.SUBTRACTION, duration)

    operator fun minus(duration: String) = minus(StringDuration(duration))

    override fun toString(): String
}
