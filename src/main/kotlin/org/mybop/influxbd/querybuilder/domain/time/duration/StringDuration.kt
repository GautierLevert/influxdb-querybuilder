package org.mybop.influxbd.querybuilder.domain.time.duration

class StringDuration(private val duration: String) : Duration {
    override fun toString() = duration
}
