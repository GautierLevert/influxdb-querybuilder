package org.mybop.influxbd.querybuilder.domain.function.aggregation

import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Mean(val selection: FunctionArg) : Function, Derivable {

    override fun toString() = "MEAN($selection)"
}
