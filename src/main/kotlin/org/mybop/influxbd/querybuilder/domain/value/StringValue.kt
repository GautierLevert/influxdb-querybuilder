package org.mybop.influxbd.querybuilder.domain.value

class StringValue(val value: String) : Value {
    override fun toString(): String = "'$value'"
}
