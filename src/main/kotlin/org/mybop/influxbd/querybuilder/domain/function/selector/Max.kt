package org.mybop.influxbd.querybuilder.domain.function.selector

import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Max(
        private val field: FunctionArg
) : Function, Derivable {

    override fun toString() = "MAX($field)"
}
