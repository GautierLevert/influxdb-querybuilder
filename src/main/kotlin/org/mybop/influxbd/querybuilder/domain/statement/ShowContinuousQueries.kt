package org.mybop.influxbd.querybuilder.domain.statement

class ShowContinuousQueries {

    override fun toString() = "SHOW CONTINUOUS QUERIES"
}
