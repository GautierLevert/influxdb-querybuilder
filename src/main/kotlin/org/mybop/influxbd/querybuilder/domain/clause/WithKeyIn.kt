package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.Tag

class WithKeyIn(
        tagKey: Tag,
        vararg others: Tag
) : WithKey {

    val tagKeys = listOf(tagKey, *others)

    override fun toString() = "WITH KEY IN (${tagKeys.joinToString(", ")})"
}
