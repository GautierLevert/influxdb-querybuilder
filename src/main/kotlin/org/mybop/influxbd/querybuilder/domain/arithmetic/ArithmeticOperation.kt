package org.mybop.influxbd.querybuilder.domain.arithmetic

class ArithmeticOperation(
        private val left: ArithmeticValue,
        private val right: ArithmeticValue,
        private val operator: ArithmeticOperator
) : ArithmeticValue {

    override fun toString(): String {
        val sb = StringBuilder()

        if (left is ArithmeticOperation) {
            sb.append('(').append(left).append(')')
        } else {
            sb.append(left)
        }

        sb.append(' ').append(operator).append(' ')

        if (right is ArithmeticOperation) {
            sb.append('(').append(right).append(')')
        } else {
            sb.append(right)
        }

        return sb.toString()
    }
}
