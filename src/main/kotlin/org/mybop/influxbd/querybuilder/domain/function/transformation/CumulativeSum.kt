package org.mybop.influxbd.querybuilder.domain.function.transformation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class CumulativeSum(
        val field: FunctionArg
) : Function {

    override fun toString() = "CUMULATIVE_SUM($field)"
}
