package org.mybop.influxbd.querybuilder.domain.function

interface Derivable {
    override fun toString(): String
}
