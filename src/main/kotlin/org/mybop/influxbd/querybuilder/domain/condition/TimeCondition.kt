package org.mybop.influxbd.querybuilder.domain.condition

import org.mybop.influxbd.querybuilder.domain.time.instant.Instant

class TimeCondition(
        val operator: TimeOperator,
        val right: Instant
) : Condition {

    override fun toString() = "time $operator $right"
}
