package org.mybop.influxbd.querybuilder.domain.function.aggregation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

/**
 *  Returns the unique field values associated with the field key.
 */
class Distinct(private val selection: FunctionArg) : Function {

    fun count() = CountDistinct(this)

    override fun toString() = "DISTINCT($selection)"
}
