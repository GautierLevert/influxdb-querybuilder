package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class DropContinuousQuery(
        val queryName: Name,
        val on: On
) {

    override fun toString(): String {
        return "DROP CONTINUOUS QUERY $queryName $on"
    }
}
