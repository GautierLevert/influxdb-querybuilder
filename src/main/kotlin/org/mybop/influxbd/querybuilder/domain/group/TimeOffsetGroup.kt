package org.mybop.influxbd.querybuilder.domain.group

import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

class TimeOffsetGroup(private val duration: Duration, private val offset: Duration) : Dimension {
    override fun toString(): String = "time($duration, $offset)"
}
