package org.mybop.influxbd.querybuilder.domain.group

interface Dimension {

    override fun toString(): String
}
