package org.mybop.influxbd.querybuilder.domain.group

import org.mybop.influxbd.querybuilder.domain.time.duration.Duration
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class TimeGroup(private val duration: Duration) : Dimension {

    infix fun offset(offset: Duration) = TimeOffsetGroup(duration, offset)

    infix fun offset(offset: String) = offset(StringDuration(offset))

    override fun toString() = "time($duration)"
}
