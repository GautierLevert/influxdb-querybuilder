package org.mybop.influxbd.querybuilder.domain.statement

class ShowUsers {

    override fun toString() = "SHOW USERS"
}
