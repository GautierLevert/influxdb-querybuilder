package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.clause.On

class ShowRetentionPolicies(
        val on: On? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("SHOW RETENTION POLICIES")

        on?.also {
            sb.append(" $it")
        }

        return sb.toString()
    }
}
