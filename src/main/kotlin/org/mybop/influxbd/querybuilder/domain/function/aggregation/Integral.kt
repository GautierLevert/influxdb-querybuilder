package org.mybop.influxbd.querybuilder.domain.function.aggregation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

/**
 * InfluxDB calculates the area under the curve for subsequent field values and converts those results into the summed area per unit. The unit argument is an integer followed by a duration literal and it is optional. If the query does not specify the unit, the unit defaults to one second (1s).
 */
class Integral(
        val selection: FunctionArg,
        val unit: Duration? = null
) : Function {

    override fun toString() = unit?.let { "INTEGRAL($selection, $it)" } ?: "INTEGRAL($selection)"
}
