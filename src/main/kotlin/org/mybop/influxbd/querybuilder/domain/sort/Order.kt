package org.mybop.influxbd.querybuilder.domain.sort

enum class Order {
    ASC,
    DESC
}
