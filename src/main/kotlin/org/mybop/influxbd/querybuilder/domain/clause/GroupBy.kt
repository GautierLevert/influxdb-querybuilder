package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.group.Dimension
import org.mybop.influxbd.querybuilder.domain.group.FillOption

class GroupBy(
        dimension: Dimension,
        vararg others: Dimension,
        var fill: FillOption? = null
) {

    val dimensions = listOf(dimension, *others)

    override fun toString(): String {
        val sb = StringBuilder("GROUP BY ${dimensions.joinToString(", ")}")

        fill?.also {
            sb.append(" fill($it)")
        }

        return sb.toString()
    }
}
