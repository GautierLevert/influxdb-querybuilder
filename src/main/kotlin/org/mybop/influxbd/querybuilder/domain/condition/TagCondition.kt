package org.mybop.influxbd.querybuilder.domain.condition

import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class TagCondition(
        val tagKey: Tag,
        val operator: TagOperator,
        val value: StringValue
) : Condition {

    override fun toString(): String =
            "$tagKey $operator $value"
}
