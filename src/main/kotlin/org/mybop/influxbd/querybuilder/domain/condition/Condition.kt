package org.mybop.influxbd.querybuilder.domain.condition

interface Condition {

    fun and(condition: Condition, vararg others: Condition) = JoinCondition(JoinOperator.AND, this, condition, *others)

    infix fun and(condition: Condition) = and(condition, *emptyArray())

    fun or(condition: Condition, vararg others: Condition) = JoinCondition(JoinOperator.OR, this, condition, *others)

    infix fun or(condition: Condition) = or(condition, *emptyArray())

    override fun toString(): String
}
