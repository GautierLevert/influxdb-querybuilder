package org.mybop.influxbd.querybuilder.domain.sort

import org.mybop.influxbd.querybuilder.domain.Field

class SortField(
        val field: Field,
        val order: Order? = null
) : Sort {
    override fun toString(): String {
        val sb = StringBuilder("$field")

        order?.also {
            sb.append(" $it")
        }

        return sb.toString()
    }
}
