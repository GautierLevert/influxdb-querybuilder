package org.mybop.influxbd.querybuilder.domain.function.transformation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

class NonNegativeDerivative(
        val field: FunctionArg,
        val unit: Duration? = null
) : Function {
    override fun toString() = unit?.let { "NON_NEGATIVE_DERIVATIVE($field, $it)" } ?: "NON_NEGATIVE_DERIVATIVE($field)"
}
