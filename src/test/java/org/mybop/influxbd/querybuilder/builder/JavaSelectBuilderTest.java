package org.mybop.influxbd.querybuilder.builder;

import org.junit.Test;
import org.mybop.influxbd.querybuilder.domain.statement.Select;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mybop.influxbd.querybuilder.builder.FunctionExtensionsKt.last;
import static org.mybop.influxbd.querybuilder.builder.MeasurementExtensionsKt.measurement;
import static org.mybop.influxbd.querybuilder.builder.MeasurementExtensionsKt.measurementDefaultRetention;
import static org.mybop.influxbd.querybuilder.builder.SelectExtensionsKt.select;
import static org.mybop.influxbd.querybuilder.builder.TimeExtensionsKt.time;
import static org.mybop.influxbd.querybuilder.builder.ValueExtentionsKt.field;

public class JavaSelectBuilderTest {

    @Test
    public void allFieldsFromSingleMeasurement() {
        //  Select all fields and tags from a single measurement
        final Select statement = select()
                .allFields()
                .from("h2o_feet")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\"");
    }

    @Test
    public void specificFields() {
        // Select specific tags and fields from a single measurement
        final Select statement = select()
                .fields("level description", "location", "water_level")
                .from("h2o_feet")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT \"level description\", \"location\", \"water_level\" FROM \"h2o_feet\"");
    }

    @Test
    public void basicArithmetic() {
        // Select a specific field from a measurement and perform basic arithmetic
        final Select statement = select()
                .fields(
                        field("water_level").times(2).plus(4)
                )
                .from("h2o_feet")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT (\"water_level\" * 2) + 4 FROM \"h2o_feet\"");
    }

    @Test
    public void moreThanOneMeasurement() {
        // Select all data from more than one measurement
        final Select statement = select()
                .from("h2o_feet", "h2o_pH")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\", \"h2o_pH\"");
    }

    @Test
    public void fullyQualifiedMeasurement() {
        // Select all data from a fully qualified measurement
        final Select statement = select()
                .from(measurement("NOAA_water_database", "autogen", "h2o_feet"))
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"NOAA_water_database\".\"autogen\".\"h2o_feet\"");
    }

    @Test
    public void defaultMeasurement() {
        // Select all data from a measurement in a particular database
        final Select statement = select()
                .from(measurementDefaultRetention("NOAA_water_database", "h2o_feet"))
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"NOAA_water_database\"..\"h2o_feet\"");
    }

    @Test
    public void fieldsWithAliases() {
        final Select statement = select()
                .fields(
                        time().alias("receivedAt"),
                        field("createdAt"),
                        field("contract"),
                        last("reportingPacket").alias("reportingPacket")
                )
                .from(measurement("retention", "measurement"))
                .groupBy("contract")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT \"time\" AS \"receivedAt\", \"createdAt\", \"contract\", LAST(\"reportingPacket\") AS \"reportingPacket\" FROM \"retention\".\"measurement\" GROUP BY \"contract\"");
    }
}
