package org.mybop.influxbd.querybuilder.builder;

import org.junit.Test;
import org.mybop.influxbd.querybuilder.domain.statement.Select;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mybop.influxbd.querybuilder.builder.FunctionExtensionsKt.mean;
import static org.mybop.influxbd.querybuilder.builder.SelectExtensionsKt.select;

public class JavaGroupByBuilderTest {
    @Test
    public void example1() {
        final Select statement = select()
                .fields(
                        mean("water_level")
                )
                .from("h2o_feet")
                .groupBy("location")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" GROUP BY \"location\"");
    }

    @Test
    public void example2() {
        final Select statement = select()
                .fields(
                        mean("index")
                )
                .from("h2o_quality")
                .groupBy("location", "randtag")
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"index\") FROM \"h2o_quality\" GROUP BY \"location\", \"randtag\"");
    }

    @Test
    public void example3() {
        final Select statement = select()
                .fields(
                        mean("index")
                )
                .from("h2o_quality")
                .groupByAllTags()
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"index\") FROM \"h2o_quality\" GROUP BY *");
    }
}
