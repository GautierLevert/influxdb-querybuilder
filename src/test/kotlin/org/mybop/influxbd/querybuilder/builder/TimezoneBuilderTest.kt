package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class TimezoneBuilderTest {

    @Test
    fun example1() {
        val statement = select {
            fields("water_level")

            from("h2o_feet")

            where(tag("location") isEqualTo "santa_monica"
                    and (time() isAfter "2015-08-18T00:00:00Z")
                    and (time() isBefore "2015-08-18T00:18:00Z")
            )

            timezone("America/Chicago")
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" WHERE \"location\" = 'santa_monica' AND time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:18:00Z' tz('America/Chicago')")
    }
}
