package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class OrderByBuilderTest {

    @Test
    fun example1() {
        val statement = select {
            fields(
                    "water_level"
            )

            from("h2o_feet")

            where(tag("location") isEqualTo "santa_monica")

            orderBy(
                    time().desc()
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" WHERE \"location\" = 'santa_monica' ORDER BY time DESC")
    }

    @Test
    fun example2() {
        val statement = select {
            fields(
                    mean("water_level")
            )

            from("h2o_feet")

            where(time() isAfter "2015-08-18T00:00:00Z" and (time() isBefore "2015-08-18T00:42:00Z"))

            groupBy(
                    time("12m")
            )

            orderBy(
                    time().desc()
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" WHERE time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:42:00Z' GROUP BY time(12m) ORDER BY time DESC")
    }
}
