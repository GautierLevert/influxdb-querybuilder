package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class DeleteBuilderTest {

    @Test
    fun simpleDelete() {
        val statement = delete {
            from("h2o_feet")
        }

        assertThat(statement.toString()).isEqualTo("DELETE FROM \"h2o_feet\"")
    }

    @Test
    fun basicWhere() {
        val statement = delete {
            from("h2o_feet")

            where(tag("location") isNotEqualTo "santa_monica"
                    and (tag("tag") isEqualTo "key"))
        }

        assertThat(statement.toString()).isEqualTo("DELETE FROM \"h2o_feet\" WHERE \"location\" != 'santa_monica' AND \"tag\" = 'key'")
    }
}
