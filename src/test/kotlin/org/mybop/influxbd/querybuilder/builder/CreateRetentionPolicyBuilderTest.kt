package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CreateRetentionPolicyBuilderTest {
    @Test
    fun example() {
        // Create a retention policy.
        val statement = createRetentionPolicy {
            named("10m.events")
            on("somedb")
            duration("60m")
            replication(2)
        }

        assertThat(statement.toString()).isEqualTo("CREATE RETENTION POLICY \"10m.events\" ON \"somedb\" DURATION 60m REPLICATION 2")
    }

    @Test
    fun example2() {
        // Create a retention policy and set it as the DEFAULT.
        val statement = createRetentionPolicy {
            named("10m.events")
            on("somedb")
            duration("60m")
            replication(2)
            default()
        }

        assertThat(statement.toString()).isEqualTo("CREATE RETENTION POLICY \"10m.events\" ON \"somedb\" DURATION 60m REPLICATION 2 DEFAULT")
    }

    @Test
    fun example3() {
        // Create a retention policy and set it as the DEFAULT.
        val statement = createRetentionPolicy {
            named("10m.events")
            on("somedb")
            duration("60m")
            replication(2)
            shardDuration("30m")
        }

        assertThat(statement.toString()).isEqualTo("CREATE RETENTION POLICY \"10m.events\" ON \"somedb\" DURATION 60m REPLICATION 2 SHARD DURATION 30m")
    }
}
