package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class DropRetentionPolicyTest {

    @Test
    fun example() {
        // drop the retention policy named 1h.cpu from mydb
        val statement = DropRetentionPolicy(
                policy = Name("1h.cpu"),
                on = On(Name("mydb"))
        )

        assertThat(statement.toString()).isEqualTo("DROP RETENTION POLICY \"1h.cpu\" ON \"mydb\"")
    }
}
