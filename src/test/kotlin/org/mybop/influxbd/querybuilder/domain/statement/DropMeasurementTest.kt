package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name

class DropMeasurementTest {

    @Test
    fun example() {
        // drop the cpu measurement

        val statement = DropMeasurement(
                measurement = Measurement(measurementName = Name("cpu"))
        )

        assertThat(statement.toString()).isEqualTo("DROP MEASUREMENT \"cpu\"")
    }
}
