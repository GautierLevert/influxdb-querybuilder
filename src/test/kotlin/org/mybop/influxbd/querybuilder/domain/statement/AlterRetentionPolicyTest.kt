package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Key
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class AlterRetentionPolicyTest {

    @Test
    fun example() {
        // Set default retention policy for mydb to 1h.cpu.
        val statement = AlterRetentionPolicy(
                policyName = Name("1h.cpu"),
                on = On(Name("mydb")),
                default = true
        )

        assertThat(statement.toString()).isEqualTo("ALTER RETENTION POLICY \"1h.cpu\" ON \"mydb\" DEFAULT")
    }

    @Test
    fun example2() {
        // Change duration and replication factor.
        val statement = AlterRetentionPolicy(
                policyName = Name("policy1"),
                on = On(Name("somedb")),
                duration = StringDuration("1h"),
                replication = 4
        )

        assertThat(statement.toString()).isEqualTo("ALTER RETENTION POLICY \"policy1\" ON \"somedb\" DURATION 1h REPLICATION 4")
    }
}
