package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class ShowRetentionPoliciesTest {
    @Test
    fun example() {
        // show all retention policies on a database
        val statement = ShowRetentionPolicies(On(Name("mydb")))

        assertThat(statement.toString()).isEqualTo("SHOW RETENTION POLICIES ON \"mydb\"")
    }
}
