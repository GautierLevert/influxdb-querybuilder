package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Field
import org.mybop.influxbd.querybuilder.domain.sort.Order
import org.mybop.influxbd.querybuilder.domain.sort.SortField

class OrderByTest {
    @Test
    fun example() {
        val clause = OrderBy(
                SortField(Field("value"), Order.DESC),
                SortField(Field("name"), Order.ASC),
                SortField(Field("time"))
        )

        assertThat(clause.toString()).isEqualTo("ORDER BY \"value\" DESC, \"name\" ASC, \"time\"")
    }
}
