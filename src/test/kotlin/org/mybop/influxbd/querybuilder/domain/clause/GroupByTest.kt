package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.group.TimeGroup
import org.mybop.influxbd.querybuilder.domain.selection.Star
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration
import org.mybop.influxbd.querybuilder.domain.value.IntValue

class GroupByTest {
    @Test
    fun example() {
        val clause = GroupBy(Tag("region"), TimeGroup(StringDuration("1d")), fill = IntValue(0))

        assertThat(clause.toString()).isEqualTo("GROUP BY \"region\", time(1d) fill(0)")
    }

    @Test
    fun example2() {
        val clause = GroupBy(Star)

        assertThat(clause.toString()).isEqualTo("GROUP BY *")
    }
}
