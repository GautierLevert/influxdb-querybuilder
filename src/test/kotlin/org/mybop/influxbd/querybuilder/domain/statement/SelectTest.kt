package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement
import org.mybop.influxbd.querybuilder.domain.clause.GroupBy
import org.mybop.influxbd.querybuilder.domain.selection.Fields
import org.mybop.influxbd.querybuilder.domain.time.StringTimezone
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class SelectTest {

    @Test
    fun example() {
        // Select from measurements grouped by the day with a timezone

        val fields: Fields = mock {
            on { toString() } doReturn "mean(\"value\")"
        }

        val groupBy: GroupBy = mock {
            on { toString() } doReturn "GROUP BY region, time(1d) fill(0)"
        }

        val statement = Select(
                fields = fields,
                from = FromMeasurement(Measurement(Name("cpu"))),
                into = Measurement(Name("cpu_1h")),
                groupBy = groupBy,
                timezone = StringTimezone(StringValue("America/Chicago"))
        )

        assertThat(statement.toString()).isEqualTo("SELECT mean(\"value\") INTO \"cpu_1h\" FROM \"cpu\" GROUP BY region, time(1d) fill(0) tz('America/Chicago')")
    }
}
