package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.Privilege
import org.mybop.influxbd.querybuilder.domain.clause.On

class GrantTest {
    @Test
    fun example() {
        // grant admin privileges

        val statement = Grant(Privilege.ALL, Name("jdoe"))

        assertThat(statement.toString()).isEqualTo("GRANT ALL TO \"jdoe\"")
    }

    @Test
    fun example2() {
        // grant read access to a database

        val statement = Grant(Privilege.READ, Name("jdoe"), On(Name("mydb")))

        assertThat(statement.toString()).isEqualTo("GRANT READ ON \"mydb\" TO \"jdoe\"")
    }
}
